<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeImpartesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('se_impartes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dia1');
            $table->string('dia2');
            $table->time('horaInicio');
            $table->time('horaFin');
            $table->string('tipo');

            //Atributos llave foranea
            $table->unsignedInteger('materia_id');
            $table->unsignedInteger('aula_id');
            $table->unsignedInteger('docente_id');
            //$table->unsignedInteger('horario_id');
            //$table->unsignedInteger('dia_id');

            //Relacionando llaves foraneas
            $table->foreign('materia_id')->references('id')->on('materias');
            $table->foreign('aula_id')->references('id')->on('aulas');
            $table->foreign('docente_id')->references('id')->on('docentes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('se_impartes');
    }
}
