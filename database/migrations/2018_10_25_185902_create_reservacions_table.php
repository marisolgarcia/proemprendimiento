<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservacions', function (Blueprint $table) {
            $table->increments('id');
            $table->time('horaInicio');
            $table->time('horaFin');
            $table->unsignedInteger('cantidad');
            $table->date('fecha');


            //Atributos llave foranea
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('aula_id');


            //Relacionando llaves foraneas
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('aula_id')->references('id')->on('aulas');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservacions');
    }
}
