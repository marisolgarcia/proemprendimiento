<?php

use Illuminate\Database\Seeder;
use SiconAulaFia\Docente;

class DocentesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::Statement('TRUNCATE docentes CASCADE');

        Docente::create([
        	'nombre' => 'Ing. Numero 1'
        ]);
        Docente::create([
        	'nombre' => 'Ing. Numero 2'
        ]);
        Docente::create([
        	'nombre' => 'Ing. Numero 3'
        ]);
        Docente::create([
        	'nombre' => 'Ing. Numero 4'
        ]);
        Docente::create([
        	'nombre' => 'Ing. Numero 5'
        ]);
        Docente::create([
        	'nombre' => 'Ing. Numero 6'
        ]);
        Docente::create([
        	'nombre' => 'Ing. Numero 7'
        ]);
        Docente::create([
        	'nombre' => 'Ing. Numero 8'
        ]);
        Docente::create([
        	'nombre' => 'Ing. Numero 9'
        ]);
        Docente::create([
        	'nombre' => 'Ing. Numero 10'
        ]);
        Docente::create([
        	'nombre' => 'Ing. Numero 11'
        ]);
    }
}
