<?php

use Illuminate\Database\Seeder;
use SiconAulaFia\Edificio;

class EdificiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::Statement('TRUNCATE edificios CASCADE');
        //DB::table('edificios')->truncate();
        
        Edificio::create([
        	'nombre' => 'Edificio B',
        ]);
        Edificio::create([
        	'nombre' => 'Edificio C',
        ]);

    }
}
