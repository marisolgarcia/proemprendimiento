<?php

use Illuminate\Database\Seeder;
use SiconAulaFia\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::Statement('TRUNCATE aulas CASCADE');
        
        // La creación de datos de roles debe ejecutarse primero
        $this->call(RoleTableSeeder::class);
        // Los usuarios necesitarán los roles previamente generados
        $this->call(UserTableSeeder::class);
        $this->call([
            EdificiosTableSeeder::class,
            AulasTableSeeder::class,
            MateriasTableSeeder::class,
            DocentesTableSeeder::class
        ]);
    }
}
