<?php

use Illuminate\Database\Seeder;
use SiconAulaFia\Materia;

class MateriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('TRUNCATE materias CASCADE');

        Materia::create([
        	'nombre' => 'Materia 1 UCB',
        	'escuela' => 'Ciencias Basicas'
        ]);
        Materia::create([
        	'nombre' => 'Materia 2 UCB',
        	'escuela' => 'Ciencias Basicas'
        ]);
        Materia::create([
        	'nombre' => 'Materia 3 UCB',
        	'escuela' => 'Ciencias Basicas'
        ]);
        Materia::create([
        	'nombre' => 'Materia 4 UCB',
        	'escuela' => 'Ciencias Basicas'
        ]);
        Materia::create([
        	'nombre' => 'Materia 5 UCB',
        	'escuela' => 'Ciencias Basicas'
        ]);
        Materia::create([
            'nombre' => 'Materia 1 Arq',
            'escuela' => 'Arquitectura'
        ]);
        Materia::create([
            'nombre' => 'Materia 2 Arq',
            'escuela' => 'Arquitectura'
        ]);
        Materia::create([
            'nombre' => 'Materia 1 Alim',
            'escuela' => 'Ing. Alimentos'
        ]);
        Materia::create([
            'nombre' => 'Materia 2 Alim',
            'escuela' => 'Ing. Alimentos'
        ]);
        Materia::create([
            'nombre' => 'Materia 1 Civil',
            'escuela' => 'Ing. Civil'
        ]);
        Materia::create([
            'nombre' => 'Materia 1 Elec',
            'escuela' => 'Ing. Electrica'
        ]);
        Materia::create([
            'nombre' => 'Materia 1 Ind',
            'escuela' => 'Ing. Industrial'
        ]);
        Materia::create([
            'nombre' => 'Materia 1 Mec',
            'escuela' => 'Ing. Mecanica'
        ]);
        Materia::create([
            'nombre' => 'Materia 1 Quim',
            'escuela' => 'Ing. Quimica'
        ]);
        Materia::create([
        	'nombre' => 'Fisica 2',
        	'escuela' => 'Ciencias Basicas'
        ]);
        Materia::create([
        	'nombre' => 'Fisica 3',
        	'escuela' => 'Ciencias Basicas'
        ]);
        Materia::create([
        	'nombre' => 'Probabilidad y Estadistica',
        	'escuela' => 'Ciencias Basicas'
        ]);
        Materia::create([
        	'nombre' => 'Programacion 1',
        	'escuela' => 'Ing. Sistemas'
        ]);
        Materia::create([
        	'nombre' => 'Programacion 2',
        	'escuela' => 'Ing. Sistemas'
        ]);
        Materia::create([
        	'nombre' => 'Programacion 3',
        	'escuela' => 'Ing. Sistemas'
        ]);
        Materia::create([
        	'nombre' => 'Tecnicas de Programacion para Internet',
        	'escuela' => 'Ing. Sistemas'
        ]);
    }
}
