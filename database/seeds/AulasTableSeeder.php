<?php

use Illuminate\Database\Seeder;
use SiconAulaFia\Aula;

class AulasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::Statement('TRUNCATE aulas CASCADE');

        Aula::create([
        	'nombre' => 'B11',
        	'piso' => '1',
        	'capacidadEstudiantes' => '100',
        	'edificio_id' => '1'
        ]);
        Aula::create([
        	'nombre' => 'B21',
        	'piso' => '2',
        	'capacidadEstudiantes' => '100',
        	'edificio_id' => '1'
        ]);
        Aula::create([
        	'nombre' => 'B22',
        	'piso' => '2',
        	'capacidadEstudiantes' => '100',
        	'edificio_id' => '1'
        ]);

        Aula::create([
        	'nombre' => 'C11',
        	'piso' => '1',
        	'capacidadEstudiantes' => '100',
        	'edificio_id' => '2'
        ]);
        Aula::create([
        	'nombre' => 'C21',
        	'piso' => '2',
        	'capacidadEstudiantes' => '100',
        	'edificio_id' => '2'
        ]);
        Aula::create([
        	'nombre' => 'C22',
        	'piso' => '2',
        	'capacidadEstudiantes' => '100',
        	'edificio_id' => '2'
        ]);
    }
}
