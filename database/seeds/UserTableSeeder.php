<?php

use Illuminate\Database\Seeder;
use SiconAulaFia\User;
use SiconAulaFia\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user = Role::where('name', 'user')->first();
        $role_admin = Role::where('name', 'admin')->first();
        
        $user = new User();
        $user->name = 'Marisol';
        $user->email = 'ga11010@ues.edu.sv';
        $user->email_verified_at = '2018-11-29 22:11:36';
        $user->password = bcrypt('admin123');
        $user->save();
        $user->roles()->attach($role_admin);
        
        $user = new User();
        $user->name = 'User';
        $user->email = 'us10001@ues.edu.sv';
        $user->email_verified_at = '2018-11-29 22:11:36';
        $user->password = bcrypt('user1234');
        $user->save();
        $user->roles()->attach($role_user);
    }
}
