<?php

namespace SiconAulaFia;

use Illuminate\Database\Eloquent\Model;

class Docente extends Model
{
    public $timestamps = false;

    protected $fillable = ['nombre'];

    public function se_impartes()
    {
        return $this->hasOne('SiconAulaFia\Se_imparte');
    }
}
