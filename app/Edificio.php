<?php

namespace SiconAulaFia;

use Illuminate\Database\Eloquent\Model;

class Edificio extends Model
{
    public $timestamps = false;


    protected $fillable = ['nombre'];


    public function aulas(){
        return $this->hasMany('SiconAulaFia\Aula');
    }
}
