<?php

namespace SiconAulaFia\Http\Controllers;

use SiconAulaFia\Aula;
use SiconAulaFia\Http\Controllers\Controller;
use SiconAulaFia\Edificio;
use SiconAulaFia\Se_imparte;
use Illuminate\Http\Request;

class AulasController extends Controller
{
	public function show(Request $request){
		$aulas = Aula::orderBy('id', 'DESC')->paginate(100);

		$edificioss = Edificio::all();
		//$aulaEdificio = ddtrbt741jsf97::table('aulas')->join('edificios','edificios.id','=', 'aulas.edificio_id')->select('aulas.nombre','aulas.piso','aulas.capacidadEstudiantes','edificios.nombre as nomEdificio')->get();
		$request->user()->authorizeRoles(['admin']);
		return view('vendor.adminlte.paginas_admin.admin_aulas', compact('aulas','edificioss'));
	}

	public function create()
	{
		//$materias = Materia::all();
    	$edificioss = Edificio::all();
    	return view('vendor.adminlte.paginas_admin.admin_aulas_crear',compact('edificioss'));

    	//return view('vendor.adminlte.paginas_admin.admin_aulas_crear');

	}

	public function store()
	{

		$data= request()->validate([
			'nombre'=>'required',
			'piso'=>'required',
			'capacidadEstudiantes'=>'required',
			'edificio_id'=>'required'
		]);

		if (empty($data['nombre'])) {
			return view('vendor.adminlte.paginas_admin.admin_aulas_crear');
		}
		//dd($data);
		Aula::create([

			'nombre'=> $data['nombre'],
			'piso'=> $data['piso'],
			'capacidadEstudiantes' => $data['capacidadEstudiantes'],
			'edificio_id' => $data['edificio_id'],
			
		]);
		return redirect("admin/aulas/crear");;
	}


	public function edit(Aula $aula)
	{
		$edificioss = Edificio::all();
		return view('vendor.adminlte.paginas_admin.admin_editAulas',['aula'=>$aula],compact('edificioss'));
		//return ('bueno');
	}

	public function update(Aula $aula)
	{
		//dd('actualizado');
		$data=request()->validate([
			'nombre'=>'required',
			'piso'=>'required',
			'capacidadEstudiantes'=>'required',
			'edificio_id'=>'required'
		]);
		$aula->update($data);
		return redirect("admin/aulas");
	}
/*
	function destroy(Aula $aula)
	{
		$aula->delete();
		return redirect("admin/aulas");
	}
*/

	
	function destroy(Aula $aula)
	{
		$imparte = Se_imparte::where('aula_id',$aula->id)->first();
		if(!is_null($imparte )){
			if($imparte->aula_id == $aula->id){
				return redirect("/admin/aulas");
			}
		}
		$aula->delete();
		return redirect("admin/aulas");
	}
}
