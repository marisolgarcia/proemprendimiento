<?php

namespace SiconAulaFia\Http\Controllers;

use Illuminate\Http\Request;
use SiconAulaFia\Materia;
use SiconAulaFia\Aula;
use SiconAulaFia\Docente;
use SiconAulaFia\Se_imparte;
use SiconAulaFia\Http\Controllers\Controller;

class RelacionesController extends Controller
{

	public function show(Request $request){
		$materiasArq = Materia::where('escuela', 'Arquitectura')->get();
		$aulas = Aula::all();
		$docentes = Docente::all();
		$relaciones = Se_imparte::all();
		$request->user()->authorizeRoles(['admin']);
		//get muestra todos los registros, sin paginar
		return view('vendor.adminlte.paginas_admin.admin_relaciones', compact('materiasArq', 'aulas', 'docentes', 'relaciones'));
	}

	public function filtrar(Request $request){
		if($request->has('cbxCarrera') && $request->cbxCarrera != ""){
			$materias = Materia::where('escuela', $request->cbxCarrera)->get();
			return response()->json([
				'materias' => $materias
			]);
		}
		else{
			return response($content = "Error en especificacion de escuela, intente de nuevo", $status = 500);
		}
	}

	public function crear(Request $request){
		if($request->isMethod('post')){
			if($request->has('cbxDia1') && $request->has('cbxDia2') && $request->has('cbxBloque') && $request->has('cbxAulas') && $request->has('lblMateria') && $request->lblMateria != "" && $request->has('cbxDocentes') && $request->has('cbxTipos')){
				$dia1 = $request->cbxDia1;
				$dia2 = $request->cbxDia2;
				$bloque = $request->cbxBloque;
				$codAula = $request->cbxAulas;
				$codMateria = $request->lblMateria;
				$codDocente = $request->cbxDocentes;
				$tipo = $request->cbxTipos;

				try{
					$aula = aula::findOrFail($codAula);
				}
				catch(ModelNotFoundException $e){
					return response("Error, Aula no existe");
				}
				try{
					$materia = materia::find($codMateria);
				}
				catch(ModelNotFoundException $e){
					return response("Error, Materia no existe");
				}
				try{
					$docente = docente::find($codDocente);
				}
				catch(ModelNotFoundException $e){
					return response("Error, Dcoente no existe");
				}
				switch($bloque){
					case 1:
						$horaInicio = "6:20";
						$horaFin = "8:00";
					break;
					case 2:
						$horaInicio = "8:05";
						$horaFin = "9:45";
					break;
					case 3:
						$horaInicio = "9:50";
						$horaFin = "11:30";
					break;
					case 4:
						$horaInicio = "11:35";
						$horaFin = "13:15";
					break;
					case 5:
						$horaInicio = "13:20";
						$horaFin = "15:00";
					break;
					case 6:
						$horaInicio = "15:05";
						$horaFin = "16:45";
					break;
					case 7:
						$horaInicio = "16:50";
						$horaFin = "18:30";
					break;
					case 8:
						$horaInicio = "18:35";
						$horaFin = "20:00";
					break;
				}
				$se_impartes = se_imparte::where('materia_id', $codMateria)->where('aula_id', $codAula)->where('docente_id', $codDocente)->where('horaInicio', $horaInicio);
				if($se_impartes->first()){
					return response("Error, ya hay un registro para esta aula a esta hora con este profesor", $status=500);
				}
				else{
					$seImparte = new Se_imparte;
					$seImparte->dia1 = $dia1;
					$seImparte->dia2 = $dia2;
					$seImparte->tipo = $tipo;
					$seImparte->materia_id = $codMateria;
					$seImparte->docente_id = $codDocente;
					$seImparte->aula_id = $codAula;
					$seImparte->horaInicio = $horaInicio;
					$seImparte->horaFin = $horaFin;
					$seImparte->save();
					return response()->json([
						'codigo' => $seImparte->id,
						'horaInicio' => $horaInicio,
						'horaFin' => $horaFin,
						'aula' => $aula->nombre,
						'materia' => $materia->nombre,
						'docente' => $docente->nombre,
						'msj' => "Registro guardado correctamente!",
					]);
				}
			}
			else{
				return response($content = "Error en ingreso de datos, verificar, por favor", $status = 500);
			}
			
		}
		else{
			return redirect('admin/relaciones');

		}
	}

	public function eliminar(Request $request){
		if ($request->isMethod('post')){
			$relaciones = se_imparte::orderBy('id', 'DESC')->get();
			foreach ($relaciones as $relacion ) {
				if($request->has('' . $relacion->id)){
					//validar aca en caso de ser necesario
					Se_imparte::find($relacion->id)->delete();
				}
			}
			return redirect('admin/relaciones');
		}
		else{
			return redirect('admin/relaciones');
		}
	}
}
