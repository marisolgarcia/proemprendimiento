<?php

namespace SiconAulaFia\Http\Controllers;


use SiconAulaFia\Edificio;
use SiconAulaFia\Http\Controllers\Controller;
use Illuminate\Http\Request;


class EdificiosController extends Controller
{
	public function show(Request $request){
		$edificios = Edificio::orderBy('id', 'DESC')->paginate(100);
		$request->user()->authorizeRoles(['admin']);		
		return view('vendor.adminlte.paginas_admin.admin_edificios', compact('edificios'));
	}

	public function guardar(Request $request){
		if ($request->isMethod('post')){
			if($request->has('txtNombre') && $request->txtNombre!=""){
				$edificios = new Edificio;
				$edificios->nombre = $request->txtNombre;
				$edificios->save();
				return response()->json([
					'codigo' => $edificios->id,
					'nombre' => $edificios->nombre,
					'mensaje' => 'Edificio Guardado'
				]);
			}
			else{
				//Devolver Error por falta de nombre
				return response($content = 'Error, no ha especificado nombre', $status = 500);
			}
		}
		else{
			//Redirigir a lista de edificios
			return redirect('admin/edificios');
		}
	}

	public function editar(Request $request){
		if ($request->isMethod('post')){
			if($request->has('txtEditarNombre') && $request->has('txtEditarCodigo') && $request->txtEditarNombre != ""){
				$edificio = edificio::find($request->txtEditarCodigo);
				$edificio->nombre = $request->txtEditarNombre;
				$edificio->save();
				return response()->json([
					'codigo' => $edificio->id,
					'nombre' => $edificio->nombre,
					'mensaje' => 'Cambios realizados'
				]);
			}
			else{
				return response($content = 'Error en datos, reintentar', $status = 500);
			}
		}
		else{
			//redireccionar
			return redirect('admin/edificios');
		}
	}
	
	public function eliminar(Request $request){
		if ($request->isMethod('post')){
			$edificios = Edificio::orderBy('id', 'DESC')->paginate(100);
			foreach ($edificios as $edificio ) {
				if($request->has('chx' . $edificio->id)){
					//Eliminar
					//Incluir aca validacion de Aulas
					Edificio::find($edificio->id)->delete();
				}
			}

			//redireccionar
			return redirect('admin/edificios');
		}
		else{
			//redireccionar
			return redirect('admin/edificios');
		}
	}
}
