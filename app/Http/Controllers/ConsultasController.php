<?php

namespace SiconAulaFia\Http\Controllers;

use SiconAulaFia\Http\Controllers\Controller;
use SiconAulaFia\Aula;
use SiconAulaFia\Reservacion;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon; 
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class ConsultasController extends Controller
{
	
	//funcion para definir los bloques de horario
	public function horario($horaActual){
		$horaInicio='';
		$horaFin='';
		if($horaActual >= '06:20'and $horaActual <= '20:15'):
			if ($horaActual >= '06:20'and $horaActual < '08:00'):
				$horaInicio=Carbon::parse('06:20:00');
				$horaFin=Carbon::parse('08:00:00');
			endif;
			if ($horaActual >= '08:00'and $horaActual < '09:45'):
				$horaInicio=Carbon::parse('08:05:00');
				$horaFin=Carbon::parse('09:45:00');
			endif;
			if ($horaActual >= '09:45'and $horaActual < '11:30'):
				$horaInicio=Carbon::parse('09:50:00');
				$horaFin=Carbon::parse('11:30:00');
			endif;
			if ($horaActual >= '11:30'and $horaActual < '13:15'):
				$horaInicio=Carbon::parse('11:35:00');
				$horaFin=Carbon::parse('13:15:00');
			endif;
			if ($horaActual >= '13:15'and $horaActual < '15:00'):
				$horaInicio=Carbon::parse('13:20:00');
				$horaFin=Carbon::parse('15:00:00');
			endif;
			if ($horaActual >= '15:00'and $horaActual < '16:45'):
				$horaInicio=Carbon::parse('15:05:00');
				$horaFin=Carbon::parse('16:45:00');
			endif;
			if ($horaActual >= '16:45'and $horaActual < '18:30'):
				$horaInicio=Carbon::parse('16:50:00');
				$horaFin=Carbon::parse('18:30:00');
			endif;
			if ($horaActual >= '18:30'and $horaActual <= '20:15'):
				$horaInicio=Carbon::parse('18:35:00');
				$horaFin=Carbon::parse('20:15:00');
			endif;
		else:
			$horaInicio=Carbon::parse('00:00:00');
			$horaFin=Carbon::parse('00:00:00');
		endif;

		return [$horaInicio,$horaFin];
	}

	public function reservacion(Request $request){
		if ($request->isMethod('post')){
			$alumnos = $request->txtAlumnosPresentes; //variable para controlar la cantidad
			$capacidad = $request->txtCapacidad;
			$cantidadAlumnos=$request->txtCantidadAlumnos;
			if ($cantidadAlumnos == 1){
				$alumnosMsg="alumno";
			}
			else{
				$alumnosMsg="alumnos";
			}
			$bloque=explode("-",$request->txtBloque);
			if($request->has('txtCantidadAlumnos') and (($alumnos + $cantidadAlumnos) <= $capacidad)){
				$reservacion = new Reservacion;
				$reservacion->horaInicio=$bloque[0];
				$reservacion->horaFin=$bloque[1];
				$reservacion->cantidad = $request->txtCantidadAlumnos;
				$reservacion->fecha=Carbon::now()->format('d-m-y');
				// obtiene el usuario logueado
				$reservacion->user_id=Auth::id();
				$reservacion->aula_id=$request->txtIdAula;
				$reservacion->save();
				$mensaje="Su reservación para ".$cantidadAlumnos." ".$alumnosMsg." en el aula ".$request->txtAula." de ".$reservacion->horaInicio." a ".$reservacion->horaFin." se realizo con exito.";
				return redirect()->back()->withSuccess($mensaje);
				//return redirect('usuario/aulasActuales');
			}
			else{
				
				$mensaje="No hay cupos para ".$cantidadAlumnos." ".$alumnosMsg." en el aula ".$request->txtAula." de ".$bloque[0]." a ".$bloque[1].".";
				return redirect()->back()->withError($mensaje);
				//return response($content = 'Error, ya no hay cupos para la cantidad de alumnos que desea reservar', $status = 500);
			}
		}
		else{
			//Redirigir a lista de aulas
			return redirect('usuario/aulasActuales');
		}
	}

	public function aulasActuales(){
		
		Carbon::setUtf8(true); // para obtener el dia actual con tildes
		setlocale(LC_TIME, 'es');// para obtener el dia actual en español

		//datos de prueba
		/*$diaActual='Viernes';
		$horaActual='15:00';
		$fechaHoy='2018-08-11';*/
		
		$diaActual=ucfirst(Carbon::now()->formatLocalized('%A'));
		$horaActual=Carbon::now()->format('H:i');
		$fechaHoy=Carbon::now()->format('Y-d-m');

		//obtiene el bloque de horario
		$hora=$this->horario($horaActual);

		//hora inicio y fin que se pasa a la vista
		$horaInicio=$hora[0];
		$horaFin=$hora[1];

		//hora de inicio y fin para la consulta
		$horaInferior=$hora[0]->format('H:i:s');
		$horaSuperior=$hora[1]->format('H:i:s');

		//obtiene las aulas que estan ocupadas en el dia y hora actual
		$aulas = DB::table('aulas')
		->join('se_impartes','aulas.id', '=', 'se_impartes.aula_id')
		->where([['dia1' ,'=' , $diaActual],['horaInicio' , '=' , $horaInferior]  ,['horaFin' , '=' , $horaSuperior]])
		->orWhere([['dia2' ,'=' , $diaActual],['horaInicio' , '=' , $horaInferior]  ,['horaFin' , '=' , $horaSuperior]])
		->select('aulas.*','se_impartes.horaInicio','se_impartes.horaFin')
		->get();

		foreach ($aulas as $aula) {
			$data[] = $aula->id;
		}

		//obtiene las aulas libres
		if ($diaActual != 'Domingo' and $horaActual >= '06:20' and $horaActual < '20:15'):
			if (count($aulas) > 0):
				$aulas = Aula::orderBy('nombre', 'ASC')->whereNotIn('id', $data)->get();
			else:
				$aulas = Aula::orderBy('nombre', 'ASC')->get();
			endif;
		else:
			$aulas = [];
		endif;

		$reservaciones=Reservacion::all();

		return view('vendor.adminlte.paginas_usuario.usuario_aulasActuales',compact('aulas','horaInicio','horaFin','reservaciones','fechaHoy','horaActual','diaActual'));
	}

	public function aulasPorHorario(Request $request){
		
		Carbon::setUtf8(true); // para obtener el dia actual con tildes
		setlocale(LC_TIME, 'es');// para obtener el dia actual en español

		//datos de prueba
		/*$diaActual='Viernes';
		$horaActual='15:00';
		$fechaHoy='2018-08-11';*/
		

		$diaActual=ucfirst(Carbon::now()->formatLocalized('%A'));
		$horaActual=Carbon::now()->format('H:i');
		$fechaHoy=Carbon::now()->format('Y-d-m');

		if ($request->isMethod('post')){
			$bloque=explode("-",$request->txtHorario);
			$horaInicio=Carbon::parse($bloque[0]);
			$horaFin=Carbon::parse($bloque[1]);
			$horaInferior=Carbon::parse($bloque[0])->format('H:i:s');
			$horaSuperior=Carbon::parse($bloque[1])->format('H:i:s');
			
		}
		else{
			//obtiene el bloque de horario
			$hora=$this->horario($horaActual);
			//hora inicio y fin que se pasa a la vista
			$horaInicio=$hora[0];
			$horaFin=$hora[1];

			//hora de inicio y fin para la consulta
			$horaInferior=$hora[0]->format('H:i:s');
			$horaSuperior=$hora[1]->format('H:i:s');
		}
		
		//obtiene las aulas que estan ocupadas en el dia y hora actual
		$aulas = DB::table('aulas')
		->join('se_impartes','aulas.id', '=', 'se_impartes.aula_id')
		->where([['dia1' ,'=' , $diaActual],['horaInicio' , '=' , $horaInferior]  ,['horaFin' , '=' , $horaSuperior]])
		->orWhere([['dia2' ,'=' , $diaActual],['horaInicio' , '=' , $horaInferior]  ,['horaFin' , '=' , $horaSuperior]])
		->select('aulas.*','se_impartes.horaInicio','se_impartes.horaFin')
		->get();

		foreach ($aulas as $aula) {
			$data[] = $aula->id;
		}

		//obtiene las aulas libres
		if ($diaActual != 'Domingo' and $horaActual >= '06:20' and $horaActual < '20:15'):
			if (count($aulas) > 0):
				$aulas = Aula::orderBy('nombre', 'ASC')->whereNotIn('id', $data)->get();
			else:
				$aulas = Aula::orderBy('nombre', 'ASC')->get();
			endif;
		else:
			$aulas = [];
		endif;

		$reservaciones=Reservacion::all();

		return view('vendor.adminlte.paginas_usuario.usuario_aulasPorHorario',compact('aulas','horaInicio','horaFin','reservaciones','fechaHoy','horaActual','diaActual'));
	}
}