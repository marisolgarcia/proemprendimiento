<?php

namespace SiconAulaFia\Http\Controllers;

use Illuminate\Http\Request;
use SiconAulaFia\Materia;
use SiconAulaFia\Se_imparte;

class materiaController extends Controller
{

	public function index(Request $request)
	{
		$materia = Materia::all();
		$request->user()->authorizeRoles(['admin']);
		return view('vendor.adminlte.paginas_admin.admin_listaMateria',compact('materia'));
	}

	public function show($id)
	{
		$materia = Materia::find($id);
		return view('vendor.adminlte.paginas_admin.admin_showMateria',compact('materia'));

		//dd($materia);
	}


	public function create()
	{
    	//$materias = Materia::all();
    	return view('vendor.adminlte.paginas_admin.admin_crearMateria');

	}

	public function store()
	{

		$data= request()->validate([
			'nombre'=>'required',
			'escuela'=>'required'
		]);

		if (empty($data['nombre'])) {
			return view('vendor.adminlte.paginas_admin.admin_crearMateria');
		}
		//dd($data);
		Materia::create([
			'nombre'=> $data['nombre'],
			'escuela'=> $data['escuela']
		]);
		return redirect("admin/materiaList");
	}


	public function edit(Materia $materia)
	{
		return view('vendor.adminlte.paginas_admin.admin_editMateria',['materia'=>$materia]);
	}

	public function update(Materia $materia)
	{
		//dd('actualizado');
		$data=request()->validate([
			'nombre'=>'required',
			'escuela'=>'required',]);
		$materia->update($data);
		return redirect("admin/materia/{$materia->id}");
	}

	function destroy(Materia $materia)
	{
		$imparte = Se_imparte::where('materia_id',$materia->id)->first();
		//$imparte->id;
		//dd($imparte);
		if(!is_null($imparte )){
		if($imparte->materia_id == $materia->id){
			return redirect("admin/materiaList");
			}
		}
		$materia->delete();
		return redirect("admin/materiaList");
	}


}

