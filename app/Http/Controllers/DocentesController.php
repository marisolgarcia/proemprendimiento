<?php

namespace SiconAulaFia\Http\Controllers;

use SiconAulaFia\Docente;
use SiconAulaFia\Se_imparte;
use SiconAulaFia\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DocentesController extends Controller
{
	public function show(Request $request){
		$docentes = Docente::orderBy('id', 'DESC')->paginate(100);
		$request->user()->authorizeRoles(['admin']);
		return view('vendor.adminlte.paginas_admin.admin_docentes', compact('docentes'));
	}

	public function create()
	{
    	//$materias = Materia::all();
    	return view('vendor.adminlte.paginas_admin.admin_docentes_crear');

	}

	public function store()
	{

		$data= request()->validate([
			'nombre'=>'required',
			
		]);

		if (empty($data['nombre'])) {
			return view('vendor.adminlte.paginas_admin.admin_profesores_crear');
		}
		//dd($data);
		Docente::create([
			'nombre'=> $data['nombre'],
			
		]);
		return redirect("admin/profesores/crear");
	}


	public function edit(Docente $docente)
	{
		return view('vendor.adminlte.paginas_admin.admin_editDocentes',['docente'=>$docente]);
		//return ('bueno');
	}

	public function update(Docente $docente)
	{
		//dd('actualizado');
		$data=request()->validate([
			'nombre'=>'required',
			
		]);
		$docente->update($data);
		return redirect("admin/profesores");
	}

	function destroy(Docente $docente)
	{
		$imparte = Se_imparte::where('docente_id',$docente->id)->first();
		if(!is_null($imparte )){
			if($imparte->docente_id == $docente->id){
				return redirect("/admin/profesores");
			}
		}
		$docente->delete();
		return redirect("admin/profesores");
	}

		/*
	function destroy(Aula $aula)
	{
		$imparte = Se_imparte::where('aula_id',$aula->id)->first();
		if(!is_null($imparte )){
			if($imparte->aula_id == $aula->id){
				return redirect("/admin/aulas");
			}
		}
		$aula->delete();
		return redirect("admin/aulas");
	}
	*/
}
