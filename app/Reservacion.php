<?php

namespace SiconAulaFia;

use Illuminate\Database\Eloquent\Model;

class Reservacion extends Model
{
    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('SiconAulaFia\User');
    }

    public function aula()
    {
        return $this->hasOne('SiconAulaFia\Aula');
    }
}
