<?php

namespace SiconAulaFia;

use Illuminate\Database\Eloquent\Model;

class Aula extends Model
{
    public $timestamps = false;

    protected $fillable = ['nombre','piso','capacidadEstudiantes','edificio_id'];

    public function edificio()
    {
        return $this->belongsTo('SiconAulaFia\Edificio');
    }

    public function se_impartes()
    {
        return $this->hasMany('SiconAulaFia\Se_imparte');
    }

    public function reservacions()
    {
        return $this->hasMany('SiconAulaFia\Reservaciones');
    }
}
