<?php

namespace SiconAulaFia;

use Illuminate\Database\Eloquent\Model;

class Se_imparte extends Model
{
   // protected $table='se_impartes';

    public $timestamps = false;

    public function aula()
    {
        return $this->belongsTo('SiconAulaFia\Aula');
    }

    public function materia()
    {
        return $this->belongsTo('SiconAulaFia\Materia');
    }

    public function docente()
    {
        return $this->belongsTo('SiconAulaFia\Docente');
    }

    public  function scopeMateria($query, $materia)
    {
        if ($materia)
            return $query->where('materia_id', 'LIKE', "%$materia%");
    }

     public  function scopeHorai($query, $horai)
    {
        if ($horai)
            return $query->where('horaInicio', 'LIKE', "%$horai%");
    }

}
