<?php

namespace SiconAulaFia;

use Illuminate\Database\Eloquent\Model;

class Materia extends Model
{
    public $timestamps = false;

    protected $fillable = ['nombre', 'escuela'];

    public function se_impartes()
    {
        return $this->hasMany('SiconAulaFia\Se_imparte');
    }

}
