@extends('adminlte::layouts.landing')

@section('landing_title')
	Home
@endsection

@section('content')
<div id="headerwrap">
    <div class="container" style="width: 100%">
        <div class="row centered align-items-center">
            <div class="col-md-8">
                <h1>Qué es AULAFINDER?</h1>
                <p>Es un Sistema Informático orientado a la web, desarrollado para beneficio del sector estudiantil y del sector docente de la Facultad de Ingenieria y Arquitectura de la Universidad de El Salvador.</p>
                <p>El principal objetivo de nuestro aplicativo es ayudar en la búsqueda de aulas que estén disponibles (vacías) en los diferentes Edificios de la facultad, para facilitar al estudiante un lugar donde pueda estudiar sin interrupciones y al docente un lugar donde pueda impartir una sesión de clases extra curricular.</p>
                <br>
                <p>También se podrán consultar las materias que están siendo impartidas en el un horario establecido, identificando en aula y el docente que la imparte.</p>
                <img class="img-responsive" src="{{ asset('/img/fiaues.jpg') }}" alt="">
                <br>
                <span>Siguenos en nuestras redes Sociales</span>
                <div class="row centered align-items-center" style="color: white; text-align: center;">
                    <div class="col-6">
                        <a href="https://www.facebook.com/AulaFinder/"><i class="fa fa-facebook fa-lg"> Facebook</i></a>
                    </div>
                    <div class="col-6">
                        <a href="https://twitter.com/AulaFinder"><i class="fa fa-twitter fa-lg"> Twitter</i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="register-box">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="register-box-body" style="width: 90%;">
                        <h3 class="login-box-msg" style="text-align: center; text-transform: uppercase;"><strong>REGISTRARSE</strong></h3>
                        <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group has-feedback">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ trans('adminlte_lang::message.fullname') }}" name="name" value="{{ old('name') }}" required autofocus>
                            <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        @if (config('auth.providers.users.field','email') === 'username')
                            <div class="form-group has-feedback">
                                <input type="text" class="form-control" placeholder="{{ trans('adminlte_lang::message.username') }}" name="username" autofocus/>
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            </div>
                        @endif
                        <div class="form-group has-feedback">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ trans('adminlte_lang::message.email') }}" name="email" value="{{ old('email') }}" required>
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="form-group has-feedback">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ trans('adminlte_lang::message.password') }}" name="password" required>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group has-feedback">
                            <input id="password-confirm" type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.retypepassword') }}" name="password_confirmation" required>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary btn-block btn-flat">
                                    {{ trans('adminlte_lang::message.register') }}
                                </button>
                            </div>
                        </div>
                    </form>

                    </div><!-- /.form-box -->
                    <br>
                </div><!-- /.register-box -->

                @include('adminlte::auth.terms')
            </div>
        </div>
    </div> <!--/ .container -->
</div><!--/ #headerwrap -->
@endsection