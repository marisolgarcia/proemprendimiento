@extends('adminlte::layouts.admin_app')

@section('htmlheader_title')
	Imparte
@endsection

@section('contentheader_title')
	Imparte
@endsection


@section('main-content')

<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">BUSQUEDA</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<h4>
						{{Form::open(['route'=>'imparteb', 'method'=>'GET','class'=>'form-inline pull-right'])}}

							<label>MATERIA</label>
							<select name="materia">
								<option value="">--escoga la materia--</option>
								@foreach($imparte as $im)
								<option value="{{$im['materia_id']}}">{{$im->materia->nombre}}</option>
								@endforeach
							</select>
							
							<label>HORA</label>
							<select name="horai">
								<option value="">--escoga la hora--</option>
								@foreach($imparte as $im)
								<option value="{{$im['horaInicio']}}">{{$im['horaInicio']}}</option>
								@endforeach
							</select>
							<button type="submit" class="btn btn-default">
								<span class="glyphicon glyphicon-search"></span>
							</button>
						{{Form::close()}}

						
					</h4>

				
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>


<h4>
				<div class="col-md-12">
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th>ID</th>
								<th>TIPO</th>
								<th>MATERIA</th>
								<th>AULA</th>
								<th>DIA1</th>
								<th>DIA2</th>
								<th>HORA INICIO</th>
								<th>HORA FIN</th>
								
							</tr>
						</thead>
						<tbody>
							@foreach($imparte as $im)
								<tr>
								<td>{{$im->id}}</td>
								<td>{{$im->tipo}}</td>
								<td>{{$im->materia->nombre}}</td>
								<td>{{$im->aula->nombre}}</td>
								<td>{{$im->dia1}}</td>
								<td>{{$im->dia2}}</td>
								<td>{{$im->horaInicio}}</td>
								<td>{{$im->horaFin}}</td>
								</tr>
								@endforeach
						</tbody>
						
					</table>
				</div>
				</h4>
@endsection
