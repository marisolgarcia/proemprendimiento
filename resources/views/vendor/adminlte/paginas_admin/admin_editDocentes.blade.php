@extends('adminlte::layouts.admin_app')

@section('htmlheader_title')
Profesor
@endsection
@section('contentheader_title')
Profesor
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Profesor</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						
						@if ($errors->any())
						<div class="alert alert-danger">
							<h6>Por favor corrige los errores de abajo</h6>
						</div>
						@endif

						
						<form method="POST" action="{{url("admin/docente/{$docente->id}")}}">
							{{method_field('PUT')}}
							{{ csrf_field()}}
						<div class="form-group">
							<label for="name">Nombre</label>
							<input type="text" name="nombre" id="name" class="form-control" placeholder="nombre" value="{{old('nombre',$docente->nombre)}}">
							@if($errors->has('nombre'))
								<p style="color:#FF0000">{{$errors->first('nombre')}}</p>
							@endif
						</div>

						<br>
							<button type="submit" class="btn btn-primary mb-2">Guardar</button>

						</form>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
