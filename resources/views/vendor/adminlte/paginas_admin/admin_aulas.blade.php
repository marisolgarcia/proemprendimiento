@extends('adminlte::layouts.admin_app')

@section('htmlheader_title')
	Aulas
@endsection

@section('contentheader_title')
Aulas
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">

		<div>
		<button type="submit" class="btn btn-primary" id="btnCrearAula" onClick="location.href='aulas/crear'">Nuevo</button>
		</div>
		
		<table id="docentes" class="table table-bordered table-striped">
			<thead>
				<tr>
					
					<th>Nombre</th>
					<th>Edificio</th>
					<th>Piso</th>
					<th>Capacidad</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				@if($aulas->count() > 0)
					@foreach ($aulas as $aula)
					<tr id="tr{{ $aula->id }}">
						
						<td>{{ $aula->nombre }}</td>  
						<td>  
							@foreach ($edificioss as $edificios)
							@if($aula->edificio_id == $edificios->id)
							{{ $edificios->nombre }} </td>  
							@else
							@endif
							@endforeach
							</td>
						<td>{{ $aula->piso }}</td>
						<td>{{ $aula->capacidadEstudiantes }}</td>

						<td><a href="aula/{{$aula->id}}/editar">editar</a>


						<form action="{{route('aula.delete', $aula)}} "method="POST">
							{{csrf_field() }}
							{{method_field('DELETE')}}
							<button type="submit" class="btn btn-danger">Eliminar</button>
						</form>
								</td>
					</tr>
					@endforeach
				@else
					<tr>
						<td colspan="4">No hay aulas registradas.</td>
					</tr>
				@endif
			</tbody>

		</table>


		
			
		
		
	</div>
@endsection
