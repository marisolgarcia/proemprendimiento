@extends('adminlte::layouts.admin_app')
@section('contentheader_title')
Aulas
@endsection
@section('htmlheader_title')
	Aulas
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Aula</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						
						@if ($errors->any())
						<div class="alert alert-danger">
							<h6>Por favor corrige los errores de abajo</h6>
						</div>
						@endif

						
						<form method="POST" action="{{url("admin/aula/{$aula->id}")}}">
							{{method_field('PUT')}}
							{{ csrf_field()}}
						
						<div class="form-group">
							<label for="name">Nombre</label>
							<input type="text" name="nombre" id="name" class="form-control" placeholder="nombre" value="{{old('nombre',$aula->nombre)}}">
							@if($errors->has('nombre'))
								<p style="color:#FF0000">{{$errors->first('nombre')}}</p>
							@endif
						</div>
						<div class="form-group">
							<label for="escuela">Piso</label>
							<input type="text" name="piso" id="piso" class="form-control" placeholder="piso" value="{{old('piso',$aula->piso)}}">
							@if($errors->has('piso'))
								<p style="color:#FF0000">{{$errors->first('piso')}}</p>
							@endif
						</div>

						<div class="form-group">
							<label for="escuela">capacidadEstudiantes</label>
							<input type="text" name="capacidadEstudiantes" id="capacidadEstudiantes" class="form-control" placeholder="capacidadEstudiantes" value="{{old('capacidadEstudiantes',$aula->capacidadEstudiantes)}}">
							@if($errors->has('capacidadEstudiantes'))
								<p style="color:#FF0000">{{$errors->first('capacidadEstudiantes')}}</p>
							@endif
						</div>

						<div class="form-group">
							<label for="escuela">Edificio</label> <!--
							<input type="text" name="edificio_id" id="edificio_id" class="form-control" placeholder="edificio_id" value="{{old('edificio_id',$aula->edificio_id)}}">   -->
							<select type="text" name="edificio_id" id="edificio_id" class="form-control">
								<option value="">-- Escoja el edificio --</option>
								@foreach($edificioss as $edificioss)
								<!-- value = "{{ old('edificio_id') }}"  
								     value = "{{ $edificioss['id'] }}"--> 
									<option value = "{{ $edificioss['id'] }}"> {{ $edificioss['nombre'] }}</option>
								@endforeach
							</select>
							@if($errors->has('edificio_id'))
								<p style="color:#FF0000">{{$errors->first('edificio_id')}}</p>
							@endif
						</div>
						<br>
							<button type="submit" class="btn btn-primary mb-2">Guardar</button>

						</form>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
