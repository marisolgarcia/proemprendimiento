@extends('adminlte::layouts.admin_app')

@section('htmlheader_title')
	Edificios
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Edificios</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<h1>Materia {{$materia->id}}</h1>
						<h4>nombre de la materia: {{$materia->nombre}}</h4>
						<h4>escuela a la que pertenece: {{$materia->escuela}}</h4>
						<a href="{{url('/materiaList/')}}">regresar a la lista de materias</a>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
