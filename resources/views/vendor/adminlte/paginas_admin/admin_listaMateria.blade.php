@extends('adminlte::layouts.admin_app')

@section('htmlheader_title')
	Materias
@endsection

@section('contentheader_title')
	Materias
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Materias</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">

						<p>
						<a href="{{route('materia.new')}}" class="btn btn-primary">Nueva Materia</a>
						</p>

						<table class="table">
  							<thead class="thead-dark">
								<tr>
									<th>NOMBRE</th>
									<th>ESCUELA</th>
									<th>ACCION</th>
								</tr>
							</thead>

							<tbody>
							@foreach($materia as $mat)
							<tr><td>
							{{$mat->nombre}}
							</td>
							<td>
							{{$mat->escuela}}
							</td>
							<td>
								<?php  //<a href="{{url('/materia/'.$mat->id)}}">ver detalles</a>?>
								<form action="{{route('materia.delete', $mat)}} "method="POST">
									{{csrf_field() }}
									{{method_field('DELETE')}}
									<a href="{{ route('materia.show',['id'=>$mat->id])}}"class="btn btn-link"><i class="glyphicon glyphicon-eye-open"></i></a>
								<a href="{{ route('materia.edit',['id' => $mat->id])}}" class="btn btn-link"><i class="glyphicon glyphicon-pencil"></i></a>
									<button type="submit" class="btn btn-link"><i class="glyphicon glyphicon-trash"></i></button>
								</form>
								
							</td>
							</tr>
							@endforeach
						</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
