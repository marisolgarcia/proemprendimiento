@extends('adminlte::layouts.admin_app')

@section('htmlheader_title')
	Relaciones
@endsection

@section('contentheader_title')
Relaciones
@endsection

@section('htmlheader')
	@parent
	<!-- Espacio para css-->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/dataTables.bootstrap.min.css') }}">
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="alert alert-success alert-dismissible" id="alerta-success" hidden="">
	                <button type="button" class="close" aria-hidden="true">&times;</button>
	                <h4><i class="icon fa fa-check"></i> Exito</h4>
	                <h5 id="mensaje_alerta"></h5>
              	</div>

              	<div class="alert alert-warning alert-dismissible" id="alerta-error" hidden="">
                	<button type="button" class="close" aria-hidden="true">&times;</button>
                	<h4><i class="icon fa fa-warning"></i> Error</h4>
                	<h5 id="mensaje_alerta2"></h5>
              	</div>
					<table style="border-collapse: separate; border-spacing: 10px;" width="100%">
						<tr>
							<td width="40%">
								<div class="form-group">
				                  <label for="cbxCarrera">Escuelas</label>
				                  <select id="cbxCarrera" class="form-control">
				                    <option>Arquitectura</option>
				                    <option>Ciencias Basicas</option>
				                    <option>Ing. Alimentos</option>
				                    <option>Ing. Civil</option>
				                    <option>Ing. Electrica</option>
				                    <option>Ing. Industrial</option>
				                    <option>Ing. Mecanica</option>
				                    <option>Ing. Quimica</option>
				                    <option>Ing. Sistemas</option>
				                  </select>
				                </div>
							</td>
							<td width="40%">
								<div class="form-group">
				                  <label for="cbxMaterias">Materias</label>
				                  <select id="cbxMaterias" class="form-control" form="frmNuevaRelacion">
				                    @foreach ($materiasArq as $materia)
				                    	<option value="{{ $materia->id }}">{{ $materia->nombre }}</option>
				                    @endforeach
				                  </select>
				                </div>
							</td>
							<td width="20%">
								<div class="form-group">
									<button id="btnNuevo" onclick="mostrarModal()" class="btn btn-default">Nuevo</button>
								</div>
							</td>
						</tr>
					</table>
					<div style="padding-top: 2%;padding-left: 90%;"><button type="submit" form="frmEliminar" class="btn btn-danger" >Eliminar</button></div>
					<div style="padding-top: 1%">
						<form method="post" id="frmEliminar" action="{{ url ('admin/eliminar_relacion')}}">
							@csrf
							<table width="100%" id="relaciones" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Hora Inicio</th>
										<th>Hora Fin</th>
										<th>Aula</th>
										<th>Materia</th>
										<th>Docente</th>
										<th>Marcar</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($relaciones as $relacion)
										<tr>
											<td>{{$relacion->horaInicio}}</td>
											<td>{{$relacion->horaFin}}</td>
											<td>{{$relacion->aula->nombre}}</td>
											<td>{{$relacion->materia->nombre}}</td>
											<td>{{$relacion->docente->nombre}}</td>
											<td><input type="checkbox" name="{{$relacion->id}}" id="{{$relacion->id}}"></td>
										</tr>
									@endforeach
								</tbody>
								<tfoot>
									<tr>
										<th>Hora Inicio</th>
										<th>Hora Fin</th>
										<th>Aula</th>
										<th>Materia</th>
										<th>Docente</th>
										<th>Marcar</th>
									</tr>
								</tfoot>
							</table>
						</form>
					</div>
			</div>
		</div>
	</div>






	<!-- Modal -->
	<div class="modal fade" id="modalNuevaRelacion">
        <div class="modal-dialog">
            <div class="modal-content">
            	<div class="modal-header">
                	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                		<span aria-hidden="true">&times;</span></button>
                		<h4 class="modal-title">Nueva Relacion</h4>
              	</div>
             	<div class="modal-body">
				<form id="frmNuevaRelacion" action="{{ url('admin/crear_relacion') }}" method="post">
					<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
             		<table width="100%" cellpadding="10">
             			<tr>
             				<td align="center" width="20%">
             					<label for="cbxDia1" ><strong>Dia 1:</strong></label>
             				</td>
             				<td align="center" width="80%">
             					<select id="cbxDia1" class="form-control">
									<option value="lunes">Lunes</option>
									<option value="martes">Martes</option>
									<option value="miercoles">Miercoles</option>
									<option value="jueves">Jueves</option>
									<option value="viernes">Viernes</option>
									<option value="sabado">Sabado</option>
								</select>
             				</td>
             			</tr>
             			<tr>
             				<td align="center" width="20%">
             					<label for="cbxDia2" ><strong>Dia 2:</strong></label>
             				</td>
             				<td align="center" width="80%">
             					<select id="cbxDia2" class="form-control">
									<option value="lunes">Lunes</option>
									<option value="martes">Martes</option>
									<option value="miercoles">Miercoles</option>
									<option value="jueves">Jueves</option>
									<option value="viernes">Viernes</option>
									<option value="sabado">Sabado</option>
								</select>
             				</td>
             			</tr>
             			<tr>
             				<td align="center" width="20%">
             					<label for="cbxBloque" ><strong>Bloque:</strong></label>
             				</td>
             				<td align="center" width="80%">
             					<select id="cbxBloque" class="form-control">
									<option value="1">6:20 - 8:00</option>
									<option value="2">8:05 - 9:45</option>
									<option value="3">9:50 - 11:30</option>
									<option value="4">11:35 - 13:15</option>
									<option value="5">13:20 - 15:00</option>
									<option value="6">15:05 - 16:45</option>
									<option value="7">16:50 - 18:30</option>
									<option value="8">18:35 - 20:15</option>
								</select>
             				</td>
             			</tr>
             			<tr>
             				<td align="center" width="20%">
             					<label for="cbxAulas" ><strong>Aula:</strong></label>
             				</td>
             				<td align="center" width="80%">
             					<select id="cbxAulas" class="form-control">
									@foreach ($aulas as $aula)
										<option value="{{ $aula->id }}">{{ $aula->nombre }}</option>
									@endforeach
								</select>
             				</td>
             			</tr>
             			<tr>
             				<td align="center" width="20%">
             					<label><strong>Materia:</strong></label>
             				</td>
             				<td align="left" width="80%">
             					<label id="lblCodigo" hidden="">asd</label>
             					<label id="lblMateria"></label>
             				</td>
             			</tr>
             			<tr>
             				<td align="center" width="20%">
             					<label for="cbxDocentes"><strong>Docente:</strong></label>
             				</td>
             				<td align="left" width="80%">
             					<select id="cbxDocentes" class="form-control">
									@foreach ($docentes as $docente)
										<option value="{{ $docente->id }}">{{ $docente->nombre }}</option>
									@endforeach
								</select>
             				</td>
             			</tr>
             			<tr>
             				<td align="center" width="20%">
             					<label for="cbxTipos"><strong>Tipos:</strong></label>
             				</td>
             				<td align="left" width="80%">
             					<select id="cbxTipos" class="form-control">
									<option value="T">Teorico</option>
									<option value="D">Discusion</option>
									<option value="L">Laboratorio</option>
								</select>
             				</td>
             			</tr>
             		</table>
         		</form>

              	</div>
	          	<div class="modal-footer">
	                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
	                <button type="submit" form="frmNuevaRelacion" id="btnModificar" class="btn btn-primary">Guardar</button>
	        	</div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection

@section('scripts')
	@parent
	<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
	<script type="text/javascript">
		
		function desplazoArriba(){
	        $("html, body").animate({ scrollTop: 0 }, "slow");
    	}


		window.onload=function(){
			$('.alert .close').on('click', function(e) {
    			$(this).parent().hide();
			});
		}

		function mostrarModal(){
			$("#modalNuevaRelacion").modal();
			document.getElementById('lblCodigo').innerHTML = $("#cbxMaterias").val();
			document.getElementById('lblMateria').innerHTML = $("#cbxMaterias option:selected").text();
		}

		$('#cbxCarrera').change(function(){
			$("#cbxMaterias").prop("disabled", true);
			$("#btnNuevo").prop("disabled", true);
			$.ajax({
				type: 'get',
				url: '{{ url ('admin/filtrar_relaciones') }}',
				data: {
					cbxCarrera : $("#cbxCarrera").val()
				},
				dataType : 'json',
				success: function(response){
					console.log(response);
					$('#cbxMaterias').empty();
					var i; 
					var cbxMaterias = document.getElementById('cbxMaterias');
					for (i = 0; i<response.materias.length; i++){
						var opcion = document.createElement("option");
						opcion.text = response.materias[i].nombre;
						opcion.value = response.materias[i].id;
						cbxMaterias.add(opcion);
					}
					$("#cbxMaterias").prop("disabled", false);
					$("#btnNuevo").prop("disabled", false);
				},
				error: function(response){
					console.log(response.responseText);
				}
			});
		});

		/*$("#frmEliminar").on("submit", function(e){
			e.preventDefault();
			var form = $("#frmEliminar");
			$.ajax({
				type : 'POST',
				data: form.serialize(),
				success : function(result){
					console.log(result);
					var alerta = $("#alerta-success");
					$("#mensaje_alerta").html(result);
					alerta.show();
				},
				error : function(result){
					console.log(result);
					var alerta = $("#alerta-error");
					$("#mensaje_alerta2").html(result.responseText);
					alerta.show();
				},
			});
		});*/

		$("#frmNuevaRelacion").on("submit", function(e){
			e.preventDefault();
			var form = $("#frmNuevaRelacion");
				$.ajax({
					type: 'POST',
					url: form.attr('action'),
					data: {
						'cbxDia1': $("#cbxDia1 option:selected").val(),
						'cbxDia2': $("#cbxDia2 option:selected").val(),
						'cbxBloque': $("#cbxBloque option:selected").val(),
						'cbxAulas': $("#cbxAulas option:selected").val(),
						'lblMateria': $("#cbxMaterias option:selected").val(),
						'cbxDocentes': $("#cbxDocentes option:selected").val(),
						'cbxTipos': $("#cbxTipos option:selected").val(),
						'_token' : $("#token").val(),
					},
					dataType : 'json',
					success: function(result){
						//Ni idea de por que NO funciona con .responseText acá
						$("#modalNuevaRelacion").modal("toggle");
						console.log(result);
						var alerta = $("#alerta-success");
						$("#mensaje_alerta").html(result.msj);
						alerta.show();
						var tabla = $("#relaciones").DataTable();
						tabla.row.add([
							result.horaInicio,
							result.horaFin,
							result.aula,
							result.materia,
							result.docente,
							'<td><input type="checkbox" id="'+result.codigo+'" name="'+result.codigo+'"></td>'
						]).draw(false);
					},
					error: function(result){
						$("#modalNuevaRelacion").modal("toggle");
						var alerta = $("#alerta-error");
						$("#mensaje_alerta2").html(result.responseText);
						alerta.show();
						console.log(result.responseText);
					}
				});	
		});
		$("#relaciones").DataTable();
	</script>
	<!-- Espacio para JS -->
@endsection