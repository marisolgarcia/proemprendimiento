@extends('adminlte::layouts.admin_app')

@section('htmlheader_title')
Profesor
@endsection

@section('contentheader_title')
Profesor
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">

		<div>
		<button type="submit" class="btn btn-primary" id="btnCrearDocente" onClick="location.href='profesores/crear'">Nuevo</button>

		</div>
		
		
		<table id="docentes" class="table table-bordered table-striped">
			<thead>
				<tr>
					
					<th>Nombre</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				@if($docentes->count() > 0)
					@foreach ($docentes as $docente)
					<tr id="tr{{ $docente->id }}">
						
						<td>{{ $docente->nombre }}</td>
						<td><a href="docente/{{$docente->id}}/editar">editar</a>
							<form action="{{route('docente.delete', $docente)}} "method="POST">
									{{csrf_field() }}
									{{method_field('DELETE')}}
									<button type="submit" class="btn btn-danger">Eliminar</button>
								</form>
								</td>
					</tr>
					@endforeach
				@else
					<tr>
						<td colspan="3">No hay docentes registrados.</td>
					</tr>
				@endif
			</tbody>

		</table>
		
		
	</div>
@endsection
