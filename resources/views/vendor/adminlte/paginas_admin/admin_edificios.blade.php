@extends('adminlte::layouts.admin_app')

@section('htmlheader_title')
	Edificios
@endsection

@section('contentheader_title')
Edificios
@endsection

@section('htmlheader')
	@parent

	<link rel="stylesheet" type="text/css" href="{{ asset('css/dataTables.bootstrap.min.css') }}">

@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				
				<div class="alert alert-success alert-dismissible" id="alerta-success" hidden="">
	                <button type="button" class="close" aria-hidden="true">&times;</button>
	                <h4><i class="icon fa fa-check"></i> Exito</h4>
	                <h5 id="mensaje_alerta"></h5>
              	</div>

              	<div class="alert alert-warning alert-dismissible" id="alerta-error" hidden="">
                	<button type="button" class="close" aria-hidden="true">&times;</button>
                	<h4><i class="icon fa fa-warning"></i> Error</h4>
                	<h5 id="mensaje_alerta2"></h5>
              	</div>
              	<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Nuevo Edificio</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
						</div>
					</div>
					<div class="box-body">
						<form method="POST" id="frmNuevo" action="{{ url('admin/edificios_guardar') }}"  class="form-horizontal">
							@csrf
							<div class="form-group">
								<label for="txtNombre" class="col-sm-2 control-label"><strong>Nombre:</strong></label>
								<div class="col-sm-8">
									<input type="text" id="txtNombre" name="txtNombre" class="form-control">
								</div>
							</div>
							<div class="box-footer">
								<button type="submit" id="btnGuardar" class="btn btn-default">Guardar</button>
							</div>
							
						</form>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
		<!-- Form Global para eliminar -->
		<form method="POST" id="frmEliminar" action="{{ url('admin/edificios_eliminar') }}">
		@csrf
		<table id="edificios" class="table table-bordered table-striped" width="100%">
			<thead>
				<tr>
					<th>Id</th>
					<th>Nombre</th>
					<th>Editar</th>
					<th>Marcar</th>
				</tr>
			</thead>
			<tbody>
				@if($edificios->count() > 0)
					@foreach ($edificios as $edificio)
					<tr id="tr{{ $edificio->id }}">
						<td>{{ $edificio->id }}</td>
						<td>{{ $edificio->nombre }}</td>
						<td><button onclick="mostrarModal(this)" id="btn{{ $edificio->id }}" class="fa fa-edit" type="button"></button></td>
						<td><input type="checkbox" id="chx{{ $edificio->id }}" name="chx{{ $edificio->id }}"></td>
					</tr>
					@endforeach
				@else
					<tr>
						<td colspan="3">No hay edificios registrados.</td>
					</tr>
				@endif
			</tbody>
			<tfoot>
				<tr>
					<th>Id</th>
					<th>Nombre</th>
					<th>Editar</th>
					<th>Marcar</th>
				</tr>
			</tfoot>
		</table>
		@if($edificios->count() > 0)
			<div>
				<button type="submit" class="btn btn-danger" id="btnEliminar">Eliminar Marcados</button>
			</div>
		@endif
		</form>
		<!-- /fin de form global para eliminado -->
	</div>


	<!-- Modal -->
	<div class="modal fade" id="modal-default">
		<form method="POST" id="frmMod" action="{{ url('admin/edificios_editar') }}" class="form-horizontal">
		  @csrf
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Editar Edificio</h4>
              </div>
              <div class="modal-body">
					<div id="modal_form"></div>
					<div class="form-group">
						<label for="txtNombre" class="col-sm-2 control-label"><strong>Nombre:</strong></label>
						<div class="col-sm-8">
							<input type="text" name="txtEditarCodigo" id="txtEditarCodigo" hidden>
							<input type="text" id="txtEditarNombre" name="txtEditarNombre" class="form-control">
						</div>
					</div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                <button type="submit" id="btnModificar" class="btn btn-primary">Guardar Cambios</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
          </form>
        </div>
        <!-- /.modal -->
@endsection

@section('scripts')
	@parent
	<script type="text/javascript">
		
		function desplazoArriba(){
	        $("html, body").animate({ scrollTop: 0 }, "slow");
    	}


		window.onload=function(){
			$('.alert .close').on('click', function(e) {
    			$(this).parent().hide();
			});
			document.getElementById('txtNombre').focus();
		}
		
		$(function(){
		    $('#modal-default').modal({
		        keyboard: true,
		        backdrop: "static",
		        show:false,
		    }).on('show', function(){
		    	console.log('prueba');
		        //var getIdFromRow = $(event.target).closest('tr').data('id');
		        //make your ajax call populate items or what even you need
		        //$(this).find('#orderDetails').html($('<b> Order Id selected: ' + getIdFromRow  + '</b>'))
		        /*var idFila = $(event.target).closest('tr').data('id');
		        $(this).find('#modal_form').html($('<b> Order Id selected: ' + idFila + '</b>'))
		        console.log(idFila);
		        var codigo = document.getElementById(idFila).cells[0].innerHTML;
		        var nombre = document.getElementById(idFila).cells[1].innerHTML;
		        document.getElementById('txtEditarNombre').value = nombre;*/
		        var datosFila = $(event.target).closest('tr').data('id');
		        console.log(datosFila);
		        document.getElementById('txtEditarNombre').value = datosFila.split('|')[1];
		    });
		});

		function mostrarModal(button){
			$("#modal-default").modal();
			var row = $(button).closest('tr');
			document.getElementById('txtEditarCodigo').value = row[0].cells[0].innerHTML;
			document.getElementById('txtEditarNombre').value = row[0].cells[1].innerHTML;
		}


		$(document).ready(function(){
			$('#btnGuardar').click(function(e){
				e.preventDefault();
				var form = $("#frmNuevo");
				$.ajax({
					type: 'POST',
					url: form.attr('action'),
					data: form.serialize(),
					success: function(result){
						//Ni idea de por que NO funciona con .responseText acá
						console.log(result);
						var alerta = $("#alerta-success");
						$("#mensaje_alerta").html(result.mensaje);
						alerta.show();

						var tabla = $("#edificios").DataTable();
						tabla.row.add([
							result.codigo,
							result.nombre,
							'<td><button onclick="mostrarModal(this)" id="btn'+ result.codigo +'" class="fa fa-edit" type="button"></button></td>',
							'<td><input type="checkbox" id="chx'+result.codigo+'" name="chx'+result.codigo+'"></td>'
						]).draw(false);
					},
					error: function(result){
						var alerta = $("#alerta-error");
						$("#mensaje_alerta2").html(result.responseText);
						alerta.show();
						console.log(result.responseText);
					}
				});	
			});


			$('#btnModificar').click(function(e){
				e.preventDefault();
				var form = $("#frmMod");
				$.ajax({
					type: 'POST',
					url: form.attr('action'),
					data: form.serialize(),
					success: function(result){
						var alerta = $("#alerta-success");
						$("#mensaje_alerta").html(result.mensaje);
						alerta.show();
						$("#modal-default").modal("toggle");

						var tabla = $("#edificios").DataTable();
						//var fila = tabla.row('#tr' + result.codigo);
						//fila.data([result.codigo, result.nombre]);
						celda = tabla.cell(tabla.row('#tr' + result.codigo), 1);
						celda.data(result.nombre);
						desplazoArriba();
						tabla.draw();
					},
					error: function(result){
						var alerta = $("#alerta-error");
						$("#mensaje_alerta2").html(result.responseText);
						alerta.show();
						desplazoArriba();
						console.log(result.responseText);
					}
				});	
			});


			$("#edificios").DataTable();
		});

	</script>
	<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
@endsection