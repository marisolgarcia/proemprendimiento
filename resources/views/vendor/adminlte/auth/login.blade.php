@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Login
@endsection

@section('content')
<body class="hold-transition login-page">
    <div id="app" v-cloak>
        <!-- Fixed navbar -->
        <div id="navigation" class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><b>AULAFINDER</b></a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                        @else
                            <li><a href="/home">{{ Auth::user()->name }}</a></li>
                        @endif
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
        <br>
        <br>
        <div class="login-box">
            <div class="login-logo">
                <a href="{{ url('/') }}"><b>AULA</b>FINDER</a>
            </div><!-- /.login-logo -->

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="login-box-body">
            <p class="login-box-msg"> INICIAR SESIÓN</p>

            <form action="{{ url('/login') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <login-input-field
                        name="{{ config('auth.providers.users.field','email') }}"
                        domain="{{ config('auth.defaults.domain','') }}"
                ></login-input-field>
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" placeholder="{{ trans('adminlte_lang::message.email') }}" name="email"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}" name="password"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input style="display:none;" type="checkbox" name="remember"> {{ trans('adminlte_lang::message.remember') }}
                            </label>
                        </div>
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('adminlte_lang::message.buttonsign') }}</button>
                    </div><!-- /.col -->
                </div>
            </form>
            <a href="{{ url('/password/reset') }}">{{ trans('adminlte_lang::message.forgotpassword') }}</a><br>

    </div>

    </div>

    <footer id="footer">
        <div id="c">
            <div class="row align-items-center">
                <div class="col">
                    <img class="rounded" width="150" height="150" class="img-responsive" src="{{ asset('/img/logoscf.png') }}">
                </div>
                <div class="col-6">
                    <p>
                        <a href="/"></a><b>AULAFINDER</b></a>
                        <strong>Copyright &copy; 2018. </strong> 
                        <br>
                        Universidad de El Salvador
                        <br>
                        Facultad de Ingeniería y arquitectura
                        <br>
                        Escuela de Ingeniería Sistemas Informáticos
                        <br>
                        Técnicas de Programación
                        <br>
                        Grupo 05
                    </p>
                </div>
                <div class="col">
                    <img class="rounded" class="img-responsive" src="{{ asset('/img/logo_ues.png') }}">
                </div>
              </div>
            </div>
        </div>
    </footer>
    </div>
    @include('adminlte::layouts.partials.scripts_auth')
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
</body>

@endsection
