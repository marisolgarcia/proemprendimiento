@extends('adminlte::layouts.admin_app')

@section('htmlheader_title')
	Aulas Actuales
@endsection

@section('htmlheader')
	@parent
	<link rel="stylesheet" type="text/css" href="{{ asset('css/dataTables.bootstrap.min.css') }}">
@endsection

@section('contentheader_title')
	@if ($diaActual != 'Domingo' and $horaActual >= '06:20' and $horaActual < '20:15')
		Aulas disponibles en el bloque:  {{$horaInicio->format('H:i')}} - {{$horaFin->format('H:i')}}
		<h5>
 		@if(session('success'))
		 <br><br>
			<div class="container-fluid spark-screen">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="alert alert-success alert-dismissible">
        					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        					{{session('success')}}
						</div>
					</div>
				</div>
			</div>
 		@endif 
		 @if(session('error')) 
		 <br><br>
			<div class="container-fluid spark-screen">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="alert alert-danger alert-dismissible">
        					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        					{{session('error')}}
						</div>
					</div>
				</div>
			</div>
 		@endif 
		</h5> 
	@else
		No hay horario asignado
	@endif
@endsection

@section('main-content')

<!--Tabla-->
@include('adminlte::paginas_usuario.usuario_reservacionTabla')

<!-- Modal -->
<div class="modal fade" id="modal-default-aulas">
	@include('adminlte::paginas_usuario.usuario_reservacionModal')
</div>
<!-- /.modal -->	
@endsection

@section('scripts')
	@parent
	<script src="{{ asset('js/consultaAulasReservacion.js') }}"></script>
	<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#aulasActuales').DataTable({'language': {'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'}});</script>
@endsection


