@extends('adminlte::layouts.admin_app')

@section('htmlheader_title')
	Aulas Por Horario
@endsection

@section('htmlheader')
	@parent
	<link rel="stylesheet" type="text/css" href="{{ asset('css/dataTables.bootstrap.min.css') }}">
@endsection

@section('contentheader_title')
	@if ($diaActual != 'Domingo' and $horaActual >= '06:20' and $horaActual < '20:15')
		Reservación de Aulas por Horario
		<br><br>
		<h5>
 		@if(session('success'))
			<div class="container-fluid spark-screen">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="alert alert-success alert-dismissible">
        					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        					{{session('success')}}
						</div>
					</div>
				</div>
			</div>
 		@endif 
		 @if(session('error')) 
			<div class="container-fluid spark-screen">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="alert alert-danger alert-dismissible">
        					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        					{{session('error')}}
						</div>
					</div>
				</div>
			</div>
 		@endif 
		</h5> 
		<h4>
		<form method="POST" action="{{ url('usuario/aulasPorHorario') }}" class="form-horizontal">
		@csrf
		<div class="form-group" style="padding-left:18%">
			<label for="bloque" class="col-sm-1 control-label"><strong>Horario:</strong></label>
			<div class="col-sm-2">
				<select class="custom-select" id="bloque" nombre="bloque">
					@if ($horaActual < '08:00')
            			<option value="06:20-08:00" @if ($horaInicio->format('H:i')=='06:20' and $horaFin->format('H:i')=='08:00') selected @endif>06:20-08:00</option>
					@endif
					@if ($horaActual < '09:45')
            			<option value="8:05-09:45" @if ($horaInicio->format('H:i')=='8:05' and $horaFin->format('H:i')=='09:45') selected @endif>8:05-09:45</option>
					@endif
					@if ($horaActual < '11:30')
            			<option value="09:50-11:30" @if ($horaInicio->format('H:i')=='09:50' and $horaFin->format('H:i')=='11:30') selected @endif>09:50-11:30</option>
					@endif
					@if ($horaActual < '13:15')
            			<option value="11:35-13:15" @if ($horaInicio->format('H:i')=='11:35' and $horaFin->format('H:i')=='13:15') selected @endif>11:35-13:15</option>
					@endif
					@if ($horaActual < '15:00')
            			<option value="13:20-15:00" @if ($horaInicio->format('H:i')=='13:20' and $horaFin->format('H:i')=='15:00') selected @endif>13:20-15:00</option>
					@endif
					@if ($horaActual < '16:45')
            			<option value="15:05-16:45" @if ($horaInicio->format('H:i')=='15:05' and $horaFin->format('H:i')=='16:45') selected @endif>15:05-16:45</option>
					@endif
					@if ($horaActual < '18:30')
            			<option value="16:50-18:30" @if ($horaInicio->format('H:i')=='16:50' and $horaFin->format('H:i')=='18:30') selected @endif>16:50-18:30</option>
					@endif
					@if ($horaActual < '20:15')
            			<option value="18:35-20:15" @if ($horaInicio->format('H:i')=='18:35' and $horaFin->format('H:i')=='20:15') selected @endif>18:35-20:15</option>
					@endif
        		</select>
			</div>
			<input type="hidden" id="txtHorario" name="txtHorario">
			<input id="consultarHorario" name="consultarHorario" type="submit" class="btn btn-primary" value="Consultar"></button>
		</div>
		</form>
		</h4>
	@else
		No hay horario asignado
	@endif
@endsection

@section('main-content')

<!--Tabla-->
@include('adminlte::paginas_usuario.usuario_reservacionTabla')

<!-- Modal -->
<div class="modal fade" id="modal-default-aulas">
	@include('adminlte::paginas_usuario.usuario_reservacionModal')
</div>
<!-- /.modal -->
@endsection

@section('scripts')
	@parent
    <script type="text/javascript">
		$('#consultarHorario').on('click',function(){
    		var valor = document.getElementById('bloque').options[document.getElementById('bloque').selectedIndex].text;
			$("#txtHorario").val(valor);
			});
	</script>
	<script src="{{ asset('js/consultaAulasReservacion.js') }}"></script>
	<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#aulasActuales').DataTable({'language': {'url': '//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'}});</script>

@endsection