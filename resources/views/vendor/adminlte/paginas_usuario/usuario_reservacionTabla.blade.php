<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
                    <!--<div class="box-header with-border">
						<h3 class="box-title">Home</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>-->
					<div class="box-body">
                        <table id="aulasActuales" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="30%">Edificio</th>
                                    <th width="15%">Piso</th>
                                    <th width="15%">Aula</th>
                                    <th width="15%">Capacidad</th>
                                    <th width="25%">Alumnos presentes</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($aulas as $aula)
									@php
										$i = 0
									@endphp
									@foreach ($reservaciones as $reservacion)
										@if ($reservacion->aula_id == $aula->id and $reservacion->horaInicio == $horaInicio->format('H:i:s') and  $reservacion->horaFin == $horaFin->format('H:i:s') and $reservacion->fecha == $fechaHoy)
											@php
												$i = $i + $reservacion->cantidad
											@endphp
										@endif
									@endforeach
                                    <tr class="open" id="tr{{ $aula->id }}"  @if ($i < $aula->capacidadEstudiantes)  data-toggle="modal" data-id="{{ $aula->id }}|{{ $aula->nombre }}|{{$horaInicio->format('H:i')}}|{{$horaFin->format('H:i')}}|{{$i}}|{{$aula->capacidadEstudiantes}}" data-target="#modal-default-aulas" @endif>
                                        <td>{{ $aula->edificio->nombre }}</td>
                                        <td>{{ $aula->piso }}</td>
                                        <td>{{ $aula->nombre }}</td>
                                        <td>{{ $aula->capacidadEstudiantes }}</td>
										<td>
											{{$i}}
										</td>
                                    </tr>
					            @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Edificio</th>
                                    <th>Piso</th>
                                    <th>Aula</th>
                                    <th>Capacidad</th>
                                    <th>Alumnos presentes</th>
                                </tr>
                            </tfoot>
                        </table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
</div>