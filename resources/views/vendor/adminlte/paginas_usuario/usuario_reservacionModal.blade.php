<div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Reservación</h4>
              </div>

			  <form method="POST" action="{{ url('usuario/reservacion') }}" class="form-horizontal">
					@csrf
              <div class="modal-body">
                
					<div id="modal_form"></div>
                    <div class="form-group">
						<label for="txtAula" class="col-sm-4 control-label"><strong>Aula:</strong></label>
						<div class="col-sm-4">
							<input type="text" id="txtAula" name="txtAula" class="form-control" readonly>
						</div>
					</div>
                    <div class="form-group">
						<label for="txtBloque" class="col-sm-4 control-label"><strong>Bloque:</strong></label>
						<div class="col-sm-4">
							<input type="text" id="txtBloque" name="txtBloque" class="form-control" readonly>
						</div>
					</div>
					<div class="form-group">
						<label for="txtCantidadAlumnos" class="col-sm-4 control-label"><strong>Cantidad Alumnos:</strong></label>
						<div class="col-sm-4">
							<input type="text" id="txtCantidadAlumnos" name="txtCantidadAlumnos" class="form-control" onkeypress="return valida(event)" min="1" pattern="^[1-9]{1}|^[1-9]{1}[0-9]+" required>
						</div>
					</div>
					<input type="hidden" id="txtIdAula" name="txtIdAula">
					<input type="hidden" id="txtAlumnosPresentes" name="txtAlumnosPresentes">
					<input type="hidden" id="txtCapacidad" name="txtCapacidad">
				
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                <input type="submit" class="btn btn-primary" value="Reservar"></button>
              </div>
			  </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->