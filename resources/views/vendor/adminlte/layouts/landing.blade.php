<!DOCTYPE html>
<!--
Landing page based on Pratt: http://blacktie.co/demo/pratt/
-->
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Sistema de Consultas de Aulas de la Facultad de Ingeniería y Arquitectura su principal objetivo es ayudar en la búsqueda de aulas que estén disponibles (vacías) en los diferentes Edificios de la facultad, para facilitar al estudiante un lugar donde pueda estudiar sin interrupciones y al docente un lugar donde pueda impartir una sesión de clases extra curricular.">
    <meta name="author" content="Roberto Castro, Patricia Solano, Cristian Castillo, Veronica Cornejo, Elena Garcia">
    <meta name="keywords" content="AULAFINDER, Aula Finder, Sistema de Consultas de Aulas, SICONAULAFIA">

    <!--Texto Enriquesido-->
    <script type='application/ld+json'> 
        {
            "@context": "http://www.schema.org",
            "@type": "WebSite",
            "name": "AulaFinder",
            "alternateName": "SICONAULAFIA",
            "url": "https://aulafinder.herokuapp.com/"
        }
     </script>

     <meta name="google-site-verification" content="3uTMcacFQ3t0Sa1KXgp7VnW6QXmyg_TWP9wmQJS8nHk" />

    <!--Protocolo opengraph-->
    <meta property="og:title" content="AULAFINDER" />
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="es_ES">
    <meta property="og:description" content="Sistema de Consultas de Aulas en la Facultad de Ingeniería y Arquitectura su principal objetivo es ayudar en la búsqueda de aulas que estén disponibles (vacías) en los diferentes Edificios de la facultad, para facilitar al estudiante un lugar donde pueda estudiar sin interrupciones y al docente un lugar donde pueda impartir una sesión de clases extra curricular." />
    <meta property="og:url" content="https://aulafinder.herokuapp.com/" />
    <meta property="og:image" content="https://aulafinder.herokuapp.com/public/img/logoscf.png" />
    <meta property="og:sitename" content="aulafinder.herokuapp.com" />

    <!--Protocolo card twitter-->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:url" content="https://aulafinder.herokuapp.com/">
    <meta name="twitter:title" content="AULAFINDER">
    <meta name="twitter:description" content="Sistema de Consultas de Aulas en la Facultad de Ingeniería y Arquitectura su principal objetivo es ayudar en la búsqueda de aulas que estén disponibles (vacías) en los diferentes Edificios de la facultad, para facilitar al estudiante un lugar donde pueda estudiar sin interrupciones y al docente un lugar donde pueda impartir una sesión de clases extra curricular.">
    <meta name="twitter:image:src" content="https://aulafinder.herokuapp.com/img/logoscf.png">
    <meta name="twitter:site" content="AulaFinder">
    <meta name="twitter:creator" content="@Roberto Castro, Patricia Solano, Cristian Castillo, Veronica Cornejo, Elena Garcia" />
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>AULAFINDER | @yield('landing_title', 'Your title here')</title>

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="{{ asset('/css/all-landing.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/all.css') }}" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>



    <script>
        //See https://laracasts.com/discuss/channels/vue/use-trans-in-vuejs
        window.trans = @php
            // copy all translations from /resources/lang/CURRENT_LOCALE/* to global JS variable
            $lang_files = File::files(resource_path() . '/lang/' . App::getLocale());
            $trans = [];
            foreach ($lang_files as $f) {
                $filename = pathinfo($f)['filename'];
                $trans[$filename] = trans($filename);
            }
            $trans['adminlte_lang_message'] = trans('adminlte_lang::message');
            echo json_encode($trans);
        @endphp
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130076845-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-130076845-1');
    </script>
</head>

<body data-spy="scroll" data-target="#navigation" data-offset="50">

<div id="app" v-cloak>
    <!-- Fixed navbar -->
    <div id="navigation">
        <div class="container" >
            <nav class="navbar navbar-default navbar-fixed-top">
                <a class="navbar-brand" href="/"><b>AULAFINDER</b></a>
                <ul class="nav navbar-nav navbar-right float-right">
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                    @else
                        <li><a href="/home">{{ Auth::user()->name }}</a></li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>

    <section id="home" name="home" class="align-items-center">
        @yield('content')
    </section>


    <footer id="footer">
        <div id="c">
            <div class="row align-items-center">
                <div class="col">
                    <img class="rounded" width="150" height="150" class="img-responsive" src="{{ asset('/img/logoscf.png') }}">
                </div>
                <div class="col-6">
                    <p>
                        <a href="/"></a><b>AULAFINDER</b></a>
                        <strong>Copyright &copy; 2018. </strong> 
                        <br>
                        Universidad de El Salvador
                        <br>
                        Facultad de Ingeniería y arquitectura
                        <br>
                        Escuela de Ingeniería Sistemas Informáticos
                        <br>
                        Técnicas de Programación
                        <br>
                        Grupo 05
                    </p>
                </div>
                <div class="col">
                    <img class="rounded" class="img-responsive" src="{{ asset('/img/logo_ues.png') }}">
                </div>
              </div>
            </div>
        </div>
    </footer>

</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ url (mix('/js/app-landing.js')) }}"></script>
<script src="{{ url (mix('/js/app.js')) }}"></script>
<script>
    $('.carousel').carousel({
        interval: 3500
    })
</script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>
