@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verifique su dirección de Email') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Un nuevo link de verificacion, ha sido enviado a su email.') }}
                        </div>
                    @endif

                    {{ __('Antes de continuar, por favor verifique su cuenta con el link de verificación enviado a su correo electrónico.') }}
                    {{ __('Si no ha recibido ningun correo') }}, <a href="{{ route('verification.resend') }}">{{ __('Haga clic aquí para solicitar otro') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
