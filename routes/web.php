<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});

Route::get('/redirect', function () {

	alert()->success('Success Mensaje', 'dfpois');
    return redirect("/materiaList");
});


//Materia
//Route::get('/materiaList','materiaController@index');
//	Route::get('/materia/{materia}','materiaController@show')->name('materia.show');
//	Route::put('/materia/{materia}','materiaController@update');
//	Route::get('/materiaNew','materiaController@create')->name('materia.new');
//	Route::post('/materiaCrear','materiaController@store');
//	Route::get('/materia/{materia}/editar','materiaController@edit')->name('materia.edit');
//	Route::delete('/materia/{materia}','materiaController@destroy')->name('materia.delete');
//	Route::get('/', 'imparteController@index')->name('imparteb');


	//poner aca todas las url que conciernen a trabajo del admin.

Route::prefix('admin')->group(function() {

	Route::get('edificios', 'EdificiosController@show')->name('edificios')->middleware('verified');
	Route::post('edificios_guardar', 'EdificiosController@guardar')->name('edificios_guardar')->middleware('verified');
	Route::post('edificios_eliminar', 'EdificiosController@eliminar')->name('edificios_eliminar')->middleware('verified');
	Route::post('edificios_editar', 'EdificiosController@editar')->name('edificios_editar')->middleware('verified');

	Route::get('relaciones', 'RelacionesController@show')->name('relaciones')->middleware('verified');
	Route::get('filtrar_relaciones', 'RelacionesController@filtrar')->name('filtrar_relaciones')->middleware('verified');
	Route::post('crear_relacion', 'RelacionesController@crear')->name('crear_relacion')->middleware('verified');
	Route::post('eliminar_relacion', 'RelacionesController@eliminar')->name('eliminar_relacion')->middleware('verified');

//vero
	Route::get('profesores', 'DocentesController@show')->name('docentes')->middleware('verified');
	Route::get('profesores/crear', 'DocentesController@create')->name('docentes_crear')->middleware('verified');
	Route::post('profesores/crear_guardar', 'DocentesController@store')->name('docenteStore')->middleware('verified');
	Route::put('/docente/{docente}','DocentesController@update')->middleware('verified');
	Route::get('docente/{docente}/editar','DocentesController@edit')->name('docente.edit')->middleware('verified');
	Route::delete('/docente/{docente}','DocentesController@destroy')->name('docente.delete')->middleware('verified');

	//Route::post('docente_eliminar', 'DocentesController@eliminar')->name('docente_eliminar');
	Route::get('aulas', 'AulasController@show')->name('aulas')->middleware('verified');
	Route::get('aulas/crear', 'AulasController@create')->name('aulas_crear')->middleware('verified');
	Route::POST('aulas/aulasNew', 'AulasController@store')->name('aulas_News')->middleware('verified');
	Route::put('/aula/{aula}','AulasController@update')->middleware('verified');
	Route::get('aula/{aula}/editar','AulasController@edit')->name('aula.edit')->middleware('verified');
	Route::delete('aula/{aula}','AulasController@destroy')->name('aula.delete')->middleware('verified');
	//Route::post('aula_eliminar', 'AulasController@eliminar')->name('aula_eliminar');

//cris
	Route::get('/materiaList','materiaController@index')->middleware('verified');
	Route::get('/materia/{materia}','materiaController@show')->name('materia.show')->middleware('verified');
	Route::put('/materia/{materia}','materiaController@update')->middleware('verified');
	Route::get('/materiaNew','materiaController@create')->name('materia.new')->middleware('verified');
	Route::post('/materiaCrear','materiaController@store')->middleware('verified');
	Route::get('/materia/{materia}/editar','materiaController@edit')->name('materia.edit')->middleware('verified');
	Route::delete('/materia/{materia}','materiaController@destroy')->name('materia.delete')->middleware('verified');
	Route::get('/', 'imparteController@index')->name('imparteb')->middleware('verified');
});

Route::prefix('usuario')->group(function() {
    Route::get('aulasActuales', 'ConsultasController@aulasActuales')->name('aulasActuales')->middleware('verified');
    Route::post('reservacion', 'ConsultasController@reservacion')->name('reservacion')->middleware('verified');
    Route::get('aulasPorHorario', 'ConsultasController@aulasPorHorario')->name('aulasPorHorario')->middleware('verified');
    Route::post('aulasPorHorario', 'ConsultasController@aulasPorHorario')->name('aulasPorHorario')->middleware('verified');
});

